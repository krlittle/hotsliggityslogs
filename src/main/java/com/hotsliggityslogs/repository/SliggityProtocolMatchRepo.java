package com.hotsliggityslogs.repository;

import com.hotsliggityslogs.models.ProtocolMatch;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface SliggityProtocolMatchRepo extends MongoRepository<ProtocolMatch, String> {
    @Query("{ 'mapName' : ?0 }")
    List<ProtocolMatch> findByMap(String mapName);
}
