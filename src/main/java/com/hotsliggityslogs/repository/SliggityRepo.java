package com.hotsliggityslogs.repository;

import com.hotsliggityslogs.models.matches.MatchDocument;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SliggityRepo extends MongoRepository<MatchDocument, String> {
    @Query("{ 'matchDate' : { $gte : ?0, $lte : ?1 }}")
    List<MatchDocument> findByDates(String beginningDate, String endDate, Sort sort);
    @Query("{ 'heroName' : ?0, 'matchDate' : { $gte : ?1, $lte : ?2 } }")
    List<MatchDocument> findByHero(String heroName, String from, String to, Sort sort);
    @Query("{ 'matchType' : ?0, 'matchDate' : { $gte : ?1, $lte : ?2 } }")
    List<MatchDocument> findByMatchType(String matchType, String from, String to, Sort sort);
    @Query("{ 'mapName' : ?0, 'matchDate' : { $gte : ?1, $lte : ?2 } }")
    List<MatchDocument> findByMap(String mapName, String from, String to, Sort sort);
    @Query("{ 'heroName' : ?0, 'matchType' : ?1, 'matchDate' : { $gte : ?2, $lte : ?3 }}")
    List<MatchDocument> findByHeroAndMatchType(String heroName, String matchType, String from, String to, Sort sort);
    @Query("{ 'heroName' : ?0, 'mapName' : ?1, 'matchDate' : { $gte : ?2, $lte : ?3 }}")
    List<MatchDocument> findByHeroAndMap(String heroName, String mapName, String from, String to, Sort sort);
    @Query("{ 'matchType' : ?0, 'mapName' : ?1, 'matchDate' : { $gte : ?2, $lte : ?3 }}")
    List<MatchDocument> findByMatchTypeAndMap(String matchType, String mapName, String from, String to, Sort sort);
    @Query("{ 'heroName' : ?0, 'mapName' : ?1, 'matchType' : ?2, 'matchDate' : { $gte : ?3, $lte : ?4 }}")
    List<MatchDocument> findByHeroAndMatchTypeAndMap(String heroName, String matchType, String mapName, String from, String to, Sort sort);
}
