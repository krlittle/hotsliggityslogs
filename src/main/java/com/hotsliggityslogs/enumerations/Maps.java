package com.hotsliggityslogs.enumerations;

/**
 * Created on 7/7/2017
 */

public enum Maps {
    BATTLEFIELD_OF_ETERNITY("Battlefield of Eternity", "BattlefieldofEternity"),
    BLACKHEARTS_BAY("Blackheart's Bay", "BlackheartsBay"),
    BRAXIS_HOLDOUT("Braxis Holdout", "BraxisHoldout"),
    CURSED_HOLLOW("Cursed Hollow", "CursedHollow"),
    DRAGON_SHIRE("Dragon Shire", "DragonShire"),
    GARDEN_OF_TERROR("Garden of Terror", "GardenofTerror"),
    HANAMURA("Hanamura", "Hanamura"),
    HAUNTED_MINES("Haunted Mines", "HauntedMines"),
    INFERNAL_SHRINES("Infernal Shrines", "InfernalShrines"),
    SKY_TEMPLE("Sky Temple", "SkyTemple"),
    TOMB_OF_THE_SPIDER_QUEEN("Tomb of the Spider Queen", "TomboftheSpiderQueen"),
    TOWERS_OF_DOOM("Towers of Doom", "TowersofDoom"),
    WARHEAD_JUNCTION("Warhead Junction", "WarheadJunction");

    private String mapName;
    private String mapIconName;

    Maps(String mapName, String mapIconName) {
        this.mapName = mapName;
        this.mapIconName = mapIconName;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public String getMapIconName() {
        return mapIconName;
    }

    public void setMapIconName(String mapIconName) {
        this.mapIconName = mapIconName;
    }

    public static String getIconByMapName(String mapName) {
        for (Maps map : Maps.values()) {
            if (mapName.equals(map.getMapName())) {
                return map.getMapIconName();
            }
        }
        return mapName;
    }

}