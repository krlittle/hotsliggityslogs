package com.hotsliggityslogs.factories;

import com.hotsliggityslogs.models.mapdata.*;
import com.hotsliggityslogs.models.matches.Match;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MapDataFactory {
    List<MapData> allMapData;
    
    BattlefieldOfEternityMapData battlefieldOfEternityMapData;
    BlackheartsBayMapData blackheartsBayMapData;
    BraxisHoldoutMapData braxisHoldoutMapData;
    CursedHollowMapData cursedHollowMapData;
    DragonShireMapData dragonShireMapData;
    GardenOfTerrorMapData gardenOfTerrorMapData;
    HanamuraMapData hanamuraMapData;
    HauntedMinesMapData hauntedMinesMapData;
    InfernalShrinesMapData infernalShrinesMapData;
    SkyTempleMapData skyTempleMapData;
    TombOfTheSpiderQueenMapData tombOfTheSpiderQueenMapData;
    TowersOfDoomMapData towersOfDoomMapData;
    WarheadJunctionMapData warheadJunctionMapData;

    public List<MapData> createMapData(List<Match> matches) {

        initialize();
        accumulateMapDataFromMatches(matches);
        calculateWinRates();

        return createListOfAllMapData();
    }

    private void accumulateMapDataFromMatches(List<Match> matches) {
        for (Match match : matches) {
            switch(match.getMapName()) {
                case "Battlefield of Eternity":
                    battlefieldOfEternityMapData.addMapData(match);
                    break;
                case "Blackheart's Bay":
                    blackheartsBayMapData.addMapData(match);
                    break;
                case "Braxis Holdout":
                    braxisHoldoutMapData.addMapData(match);
                    break;
                case "Cursed Hollow":
                    cursedHollowMapData.addMapData(match);
                    break;
                case "Dragon Shire":
                    dragonShireMapData.addMapData(match);
                    break;
                case "Garden of Terror":
                    gardenOfTerrorMapData.addMapData(match);
                    break;
                case "Hanamura":
                    hanamuraMapData.addMapData(match);
                    break;
                case "Haunted Mines":
                    hauntedMinesMapData.addMapData(match);
                    break;
                case "Infernal Shrines":
                    infernalShrinesMapData.addMapData(match);
                    break;
                case "Sky Temple":
                    skyTempleMapData.addMapData(match);
                    break;
                case "Tomb of the Spider Queen":
                    tombOfTheSpiderQueenMapData.addMapData(match);
                    break;
                case "Towers of Doom":
                    towersOfDoomMapData.addMapData(match);
                    break;
                case "Warhead Junction":
                    warheadJunctionMapData.addMapData(match);
                    break;
            }
        }
    }

    private void calculateWinRates() {
        calculateWinRatesByMap(battlefieldOfEternityMapData);
        calculateWinRatesByMap(blackheartsBayMapData);
        calculateWinRatesByMap(braxisHoldoutMapData);
        calculateWinRatesByMap(cursedHollowMapData);
        calculateWinRatesByMap(dragonShireMapData);
        calculateWinRatesByMap(gardenOfTerrorMapData);
        calculateWinRatesByMap(hanamuraMapData);
        calculateWinRatesByMap(hauntedMinesMapData);
        calculateWinRatesByMap(infernalShrinesMapData);
        calculateWinRatesByMap(skyTempleMapData);
        calculateWinRatesByMap(tombOfTheSpiderQueenMapData);
        calculateWinRatesByMap(towersOfDoomMapData);
        calculateWinRatesByMap(warheadJunctionMapData);
    }

    private void calculateWinRatesByMap(MapData mapData) {
        if(mapData.getMatches() > 0) {
            mapData.setWinRate(mapData.calculateWinRate(mapData.getWins(), mapData.getMatches()));
        } else {
            mapData.setWinRate(null);
        }
    }
    
    private List<MapData> createListOfAllMapData() {
        addMapData(battlefieldOfEternityMapData);
        addMapData(blackheartsBayMapData);
        addMapData(braxisHoldoutMapData);
        addMapData(cursedHollowMapData);
        addMapData(dragonShireMapData);
        addMapData(gardenOfTerrorMapData);
        addMapData(hanamuraMapData);
        addMapData(hauntedMinesMapData);
        addMapData(infernalShrinesMapData);
        addMapData(skyTempleMapData);
        addMapData(tombOfTheSpiderQueenMapData);
        addMapData(towersOfDoomMapData);
        addMapData(warheadJunctionMapData);

        return allMapData;
    }

    private void addMapData(MapData mapData) {
        if(mapData.getMatches() > 0) {
            allMapData.add(mapData);
        }
    }

    private void initialize() {
        allMapData = new ArrayList<>();
        
        battlefieldOfEternityMapData = new BattlefieldOfEternityMapData();
        blackheartsBayMapData = new BlackheartsBayMapData();
        braxisHoldoutMapData = new BraxisHoldoutMapData();
        cursedHollowMapData = new CursedHollowMapData();
        dragonShireMapData = new DragonShireMapData();
        gardenOfTerrorMapData = new GardenOfTerrorMapData();
        hanamuraMapData = new HanamuraMapData();
        hauntedMinesMapData = new HauntedMinesMapData();
        infernalShrinesMapData = new InfernalShrinesMapData();
        skyTempleMapData = new SkyTempleMapData();
        tombOfTheSpiderQueenMapData = new TombOfTheSpiderQueenMapData();
        towersOfDoomMapData = new TowersOfDoomMapData();
        warheadJunctionMapData = new WarheadJunctionMapData();
    }
}