package com.hotsliggityslogs.models;

public class Player {

    private String name;
    private String hero;
    private int workingSetSlotId;
    private int teamId;
    private int result;
    private TeamColor teamColor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHero() {
        return hero;
    }

    public void setHero(String hero) {
        this.hero = hero;
    }

    public int getWorkingSetSlotId() {
        return workingSetSlotId;
    }

    public void setWorkingSetSlotId(int workingSetSlotId) {
        this.workingSetSlotId = workingSetSlotId;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public TeamColor getTeamColor() {
        return teamColor;
    }

    public void setTeamColor(TeamColor teamColor) {
        this.teamColor = teamColor;
    }
}
