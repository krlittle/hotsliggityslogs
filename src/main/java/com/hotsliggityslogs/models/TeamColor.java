package com.hotsliggityslogs.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TeamColor {
    @JsonProperty("m_r")
    private int red;
    @JsonProperty("m_g")
    private int green;
    @JsonProperty("m_b")
    private int blue;
    @JsonProperty("m_a")
    private int alpha;

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }
}
