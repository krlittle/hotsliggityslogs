package com.hotsliggityslogs.models.matches;

public interface Match {
    String getId();
    void setId(String id);

    String getMatchType();
    void setMatchType(String matchType);

    String getMapName();
    void setMapName(String mapName);

    String getMatchLength();
    void setMatchLength(String matchLength);

    String getHeroName();
    void setHeroName(String heroName);

    String getHeroLevel();
    void setHeroLevel(String heroLevel);

    String getMatchmakingRating();
    void setMatchmakingRating(String matchmakingRating);

    String getRatingAdjustmentPoints();
    void setRatingAdjustmentPoints(String ratingAdjustmentPoints);

    String getMatchDate();
    void setMatchDate(String matchDate);

    String getMatchTime();
    void setMatchTime(String matchTime);

    String getMatchOutcome();
    void setMatchOutcome(String matchOutcome);

    String getSeason();
    void setSeason(String season);
}
