package com.hotsliggityslogs.models.matches;

public class AbstractMatch implements Match {
    String id;
    String matchType;
    String mapName;
    String matchLength;
    String heroName;
    String heroLevel;
    String matchmakingRating;
    String ratingAdjustmentPoints;
    String matchDate;
    String matchTime;
    String matchOutcome;
    String season;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getMatchType() {
        return matchType;
    }

    @Override
    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    @Override
    public String getMatchLength() {
        return matchLength;
    }

    @Override
    public void setMatchLength(String matchLength) {
        this.matchLength = matchLength;
    }

    @Override
    public String getHeroName() {
        return heroName;
    }

    @Override
    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    @Override
    public String getHeroLevel() {
        return heroLevel;
    }

    @Override
    public void setHeroLevel(String heroLevel) {
        this.heroLevel = heroLevel;
    }

    @Override
    public String getMatchmakingRating() {
        return matchmakingRating;
    }

    @Override
    public void setMatchmakingRating(String matchmakingRating) {
        this.matchmakingRating = matchmakingRating;
    }

    @Override
    public String getRatingAdjustmentPoints() {
        return ratingAdjustmentPoints;
    }

    @Override
    public void setRatingAdjustmentPoints(String ratingAdjustmentPoints) {
        this.ratingAdjustmentPoints = ratingAdjustmentPoints;
    }

    @Override
    public String getMatchDate() {
        return matchDate;
    }

    @Override
    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    @Override
    public String getMatchTime() {
        return matchTime;
    }

    @Override
    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    @Override
    public String getMatchOutcome() {
        return matchOutcome;
    }

    @Override
    public void setMatchOutcome(String matchOutcome) {
        this.matchOutcome = matchOutcome;
    }

    @Override
    public String getSeason() {
        return season;
    }

    @Override
    public void setSeason(String season) {
        this.season = season;
    }
}