package com.hotsliggityslogs.models.responses;

import com.hotsliggityslogs.models.ProtocolMatch;

import java.util.List;

public class ProtocolResponse {
    private List<ProtocolMatch> protocolMatches;

    public List<ProtocolMatch> getProtocolMatches() {
        return protocolMatches;
    }

    public void setProtocolMatches(List<ProtocolMatch> protocolMatches) {
        this.protocolMatches = protocolMatches;
    }
}
