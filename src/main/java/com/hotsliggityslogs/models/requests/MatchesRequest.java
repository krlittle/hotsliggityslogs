package com.hotsliggityslogs.models.requests;

import com.hotsliggityslogs.models.matches.MatchDocument;

import java.util.ArrayList;
import java.util.List;

public class MatchesRequest {
    private List<MatchDocument> matches = new ArrayList<>();

    public List<MatchDocument> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchDocument> matches) {
        this.matches = matches;
    }
}
