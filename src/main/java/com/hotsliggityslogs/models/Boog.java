package com.hotsliggityslogs.models;

/**
 * Created on 7/7/2017
 */

public class Boog {
    private int battlefieldOfEternityWins = 0;
    private int blackheartsBayWins = 0;
    private int braxisHoldoutWins = 0;
    private int cursedHollowWins = 0;
    private int dragonShireWins = 0;
    private int gardenOfTerrorWins = 0;
    private int hanamuraWins = 0;
    private int hauntedMinesWins = 0;
    private int infernalShrinesWins = 0;
    private int skyTempleWins = 0;
    private int tombOfTheSpiderQueenWins = 0;
    private int towersOfDoomWins = 0;
    private int warheadJunctionWins = 0;

    private int battlefieldOfEternityLosses = 0;
    private int blackheartsBayLosses = 0;
    private int braxisHoldoutLosses = 0;
    private int cursedHollowLosses = 0;
    private int dragonShireLosses = 0;
    private int gardenOfTerrorLosses = 0;
    private int hanamuraLosses = 0;
    private int hauntedMinesLosses = 0;
    private int infernalShrinesLosses = 0;
    private int skyTempleLosses = 0;
    private int tombOfTheSpiderQueenLosses = 0;
    private int towersOfDoomLosses = 0;
    private int warheadJunctionLosses = 0;

    private int battlefieldOfEternityMatches = 0;
    private int blackheartsBayMatches = 0;
    private int braxisHoldoutMatches = 0;
    private int cursedHollowMatches = 0;
    private int dragonShireMatches = 0;
    private int gardenOfTerrorMatches = 0;
    private int hanamuraMatches = 0;
    private int hauntedMinesMatches = 0;
    private int infernalShrinesMatches = 0;
    private int skyTempleMatches = 0;
    private int tombOfTheSpiderQueenMatches = 0;
    private int towersOfDoomMatches = 0;
    private int warheadJunctionMatches = 0;

    private double battlefieldOfEternityWinRate = 0.0;
    private double blackheartsBayWinRate = 0.0;
    private double braxisHoldoutWinRate = 0.0;
    private double cursedHollowWinRate = 0.0;
    private double dragonShireWinRate = 0.0;
    private double gardenOfTerrorWinRate = 0.0;
    private double hanamuraWinRate = 0.0;
    private double hauntedMinesWinRate = 0.0;
    private double infernalShrinesWinRate = 0.0;
    private double skyTempleWinRate = 0.0;
    private double tombOfTheSpiderQueenWinRate = 0.0;
    private double towersOfDoomWinRate = 0.0;
    private double warheadJunctionWinRate = 0.0;

    public int getBattlefieldOfEternityWins() {
        return battlefieldOfEternityWins;
    }

    public void setBattlefieldOfEternityWins(int battlefieldOfEternityWins) {
        this.battlefieldOfEternityWins = battlefieldOfEternityWins;
    }

    public int getBlackheartsBayWins() {
        return blackheartsBayWins;
    }

    public void setBlackheartsBayWins(int blackheartsBayWins) {
        this.blackheartsBayWins = blackheartsBayWins;
    }

    public int getBraxisHoldoutWins() {
        return braxisHoldoutWins;
    }

    public void setBraxisHoldoutWins(int braxisHoldoutWins) {
        this.braxisHoldoutWins = braxisHoldoutWins;
    }

    public int getCursedHollowWins() {
        return cursedHollowWins;
    }

    public void setCursedHollowWins(int cursedHollowWins) {
        this.cursedHollowWins = cursedHollowWins;
    }

    public int getDragonShireWins() {
        return dragonShireWins;
    }

    public void setDragonShireWins(int dragonShireWins) {
        this.dragonShireWins = dragonShireWins;
    }

    public int getGardenOfTerrorWins() {
        return gardenOfTerrorWins;
    }

    public void setGardenOfTerrorWins(int gardenOfTerrorWins) {
        this.gardenOfTerrorWins = gardenOfTerrorWins;
    }

    public int getHanamuraWins() {
        return hanamuraWins;
    }

    public void setHanamuraWins(int hanamuraWins) {
        this.hanamuraWins = hanamuraWins;
    }

    public int getHauntedMinesWins() {
        return hauntedMinesWins;
    }

    public void setHauntedMinesWins(int hauntedMinesWins) {
        this.hauntedMinesWins = hauntedMinesWins;
    }

    public int getInfernalShrinesWins() {
        return infernalShrinesWins;
    }

    public void setInfernalShrinesWins(int infernalShrinesWins) {
        this.infernalShrinesWins = infernalShrinesWins;
    }

    public int getSkyTempleWins() {
        return skyTempleWins;
    }

    public void setSkyTempleWins(int skyTempleWins) {
        this.skyTempleWins = skyTempleWins;
    }

    public int getTombOfTheSpiderQueenWins() {
        return tombOfTheSpiderQueenWins;
    }

    public void setTombOfTheSpiderQueenWins(int tombOfTheSpiderQueenWins) {
        this.tombOfTheSpiderQueenWins = tombOfTheSpiderQueenWins;
    }

    public int getTowersOfDoomWins() {
        return towersOfDoomWins;
    }

    public void setTowersOfDoomWins(int towersOfDoomWins) {
        this.towersOfDoomWins = towersOfDoomWins;
    }

    public int getWarheadJunctionWins() {
        return warheadJunctionWins;
    }

    public void setWarheadJunctionWins(int warheadJunctionWins) {
        this.warheadJunctionWins = warheadJunctionWins;
    }

    public int getBattlefieldOfEternityLosses() {
        return battlefieldOfEternityLosses;
    }

    public void setBattlefieldOfEternityLosses(int battlefieldOfEternityLosses) {
        this.battlefieldOfEternityLosses = battlefieldOfEternityLosses;
    }

    public int getBlackheartsBayLosses() {
        return blackheartsBayLosses;
    }

    public void setBlackheartsBayLosses(int blackheartsBayLosses) {
        this.blackheartsBayLosses = blackheartsBayLosses;
    }

    public int getBraxisHoldoutLosses() {
        return braxisHoldoutLosses;
    }

    public void setBraxisHoldoutLosses(int braxisHoldoutLosses) {
        this.braxisHoldoutLosses = braxisHoldoutLosses;
    }

    public int getCursedHollowLosses() {
        return cursedHollowLosses;
    }

    public void setCursedHollowLosses(int cursedHollowLosses) {
        this.cursedHollowLosses = cursedHollowLosses;
    }

    public int getDragonShireLosses() {
        return dragonShireLosses;
    }

    public void setDragonShireLosses(int dragonShireLosses) {
        this.dragonShireLosses = dragonShireLosses;
    }

    public int getGardenOfTerrorLosses() {
        return gardenOfTerrorLosses;
    }

    public void setGardenOfTerrorLosses(int gardenOfTerrorLosses) {
        this.gardenOfTerrorLosses = gardenOfTerrorLosses;
    }

    public int getHanamuraLosses() {
        return hanamuraLosses;
    }

    public void setHanamuraLosses(int hanamuraLosses) {
        this.hanamuraLosses = hanamuraLosses;
    }

    public int getHauntedMinesLosses() {
        return hauntedMinesLosses;
    }

    public void setHauntedMinesLosses(int hauntedMinesLosses) {
        this.hauntedMinesLosses = hauntedMinesLosses;
    }

    public int getInfernalShrinesLosses() {
        return infernalShrinesLosses;
    }

    public void setInfernalShrinesLosses(int infernalShrinesLosses) {
        this.infernalShrinesLosses = infernalShrinesLosses;
    }

    public int getSkyTempleLosses() {
        return skyTempleLosses;
    }

    public void setSkyTempleLosses(int skyTempleLosses) {
        this.skyTempleLosses = skyTempleLosses;
    }

    public int getTombOfTheSpiderQueenLosses() {
        return tombOfTheSpiderQueenLosses;
    }

    public void setTombOfTheSpiderQueenLosses(int tombOfTheSpiderQueenLosses) {
        this.tombOfTheSpiderQueenLosses = tombOfTheSpiderQueenLosses;
    }

    public int getTowersOfDoomLosses() {
        return towersOfDoomLosses;
    }

    public void setTowersOfDoomLosses(int towersOfDoomLosses) {
        this.towersOfDoomLosses = towersOfDoomLosses;
    }

    public int getWarheadJunctionLosses() {
        return warheadJunctionLosses;
    }

    public void setWarheadJunctionLosses(int warheadJunctionLosses) {
        this.warheadJunctionLosses = warheadJunctionLosses;
    }

    public int getBattlefieldOfEternityMatches() {
        return battlefieldOfEternityMatches;
    }

    public void setBattlefieldOfEternityMatches(int battlefieldOfEternityMatches) {
        this.battlefieldOfEternityMatches = battlefieldOfEternityMatches;
    }

    public int getBlackheartsBayMatches() {
        return blackheartsBayMatches;
    }

    public void setBlackheartsBayMatches(int blackheartsBayMatches) {
        this.blackheartsBayMatches = blackheartsBayMatches;
    }

    public int getBraxisHoldoutMatches() {
        return braxisHoldoutMatches;
    }

    public void setBraxisHoldoutMatches(int braxisHoldoutMatches) {
        this.braxisHoldoutMatches = braxisHoldoutMatches;
    }

    public int getCursedHollowMatches() {
        return cursedHollowMatches;
    }

    public void setCursedHollowMatches(int cursedHollowMatches) {
        this.cursedHollowMatches = cursedHollowMatches;
    }

    public int getDragonShireMatches() {
        return dragonShireMatches;
    }

    public void setDragonShireMatches(int dragonShireMatches) {
        this.dragonShireMatches = dragonShireMatches;
    }

    public int getGardenOfTerrorMatches() {
        return gardenOfTerrorMatches;
    }

    public void setGardenOfTerrorMatches(int gardenOfTerrorMatches) {
        this.gardenOfTerrorMatches = gardenOfTerrorMatches;
    }

    public int getHanamuraMatches() {
        return hanamuraMatches;
    }

    public void setHanamuraMatches(int hanamuraMatches) {
        this.hanamuraMatches = hanamuraMatches;
    }

    public int getHauntedMinesMatches() {
        return hauntedMinesMatches;
    }

    public void setHauntedMinesMatches(int hauntedMinesMatches) {
        this.hauntedMinesMatches = hauntedMinesMatches;
    }

    public int getInfernalShrinesMatches() {
        return infernalShrinesMatches;
    }

    public void setInfernalShrinesMatches(int infernalShrinesMatches) {
        this.infernalShrinesMatches = infernalShrinesMatches;
    }

    public int getSkyTempleMatches() {
        return skyTempleMatches;
    }

    public void setSkyTempleMatches(int skyTempleMatches) {
        this.skyTempleMatches = skyTempleMatches;
    }

    public int getTombOfTheSpiderQueenMatches() {
        return tombOfTheSpiderQueenMatches;
    }

    public void setTombOfTheSpiderQueenMatches(int tombOfTheSpiderQueenMatches) {
        this.tombOfTheSpiderQueenMatches = tombOfTheSpiderQueenMatches;
    }

    public int getTowersOfDoomMatches() {
        return towersOfDoomMatches;
    }

    public void setTowersOfDoomMatches(int towersOfDoomMatches) {
        this.towersOfDoomMatches = towersOfDoomMatches;
    }

    public int getWarheadJunctionMatches() {
        return warheadJunctionMatches;
    }

    public void setWarheadJunctionMatches(int warheadJunctionMatches) {
        this.warheadJunctionMatches = warheadJunctionMatches;
    }

    public double getBattlefieldOfEternityWinRate() {
        return battlefieldOfEternityWinRate;
    }

    public void setBattlefieldOfEternityWinRate(double battlefieldOfEternityWinRate) {
        this.battlefieldOfEternityWinRate = battlefieldOfEternityWinRate;
    }

    public double getBlackheartsBayWinRate() {
        return blackheartsBayWinRate;
    }

    public void setBlackheartsBayWinRate(double blackheartsBayWinRate) {
        this.blackheartsBayWinRate = blackheartsBayWinRate;
    }

    public double getBraxisHoldoutWinRate() {
        return braxisHoldoutWinRate;
    }

    public void setBraxisHoldoutWinRate(double braxisHoldoutWinRate) {
        this.braxisHoldoutWinRate = braxisHoldoutWinRate;
    }

    public double getCursedHollowWinRate() {
        return cursedHollowWinRate;
    }

    public void setCursedHollowWinRate(double cursedHollowWinRate) {
        this.cursedHollowWinRate = cursedHollowWinRate;
    }

    public double getDragonShireWinRate() {
        return dragonShireWinRate;
    }

    public void setDragonShireWinRate(double dragonShireWinRate) {
        this.dragonShireWinRate = dragonShireWinRate;
    }

    public double getGardenOfTerrorWinRate() {
        return gardenOfTerrorWinRate;
    }

    public void setGardenOfTerrorWinRate(double gardenOfTerrorWinRate) {
        this.gardenOfTerrorWinRate = gardenOfTerrorWinRate;
    }

    public double getHanamuraWinRate() {
        return hanamuraWinRate;
    }

    public void setHanamuraWinRate(double hanamuraWinRate) {
        this.hanamuraWinRate = hanamuraWinRate;
    }

    public double getHauntedMinesWinRate() {
        return hauntedMinesWinRate;
    }

    public void setHauntedMinesWinRate(double hauntedMinesWinRate) {
        this.hauntedMinesWinRate = hauntedMinesWinRate;
    }

    public double getInfernalShrinesWinRate() {
        return infernalShrinesWinRate;
    }

    public void setInfernalShrinesWinRate(double infernalShrinesWinRate) {
        this.infernalShrinesWinRate = infernalShrinesWinRate;
    }

    public double getSkyTempleWinRate() {
        return skyTempleWinRate;
    }

    public void setSkyTempleWinRate(double skyTempleWinRate) {
        this.skyTempleWinRate = skyTempleWinRate;
    }

    public double getTombOfTheSpiderQueenWinRate() {
        return tombOfTheSpiderQueenWinRate;
    }

    public void setTombOfTheSpiderQueenWinRate(double tombOfTheSpiderQueenWinRate) {
        this.tombOfTheSpiderQueenWinRate = tombOfTheSpiderQueenWinRate;
    }

    public double getTowersOfDoomWinRate() {
        return towersOfDoomWinRate;
    }

    public void setTowersOfDoomWinRate(double towersOfDoomWinRate) {
        this.towersOfDoomWinRate = towersOfDoomWinRate;
    }

    public double getWarheadJunctionWinRate() {
        return warheadJunctionWinRate;
    }

    public void setWarheadJunctionWinRate(double warheadJunctionWinRate) {
        this.warheadJunctionWinRate = warheadJunctionWinRate;
    }
}