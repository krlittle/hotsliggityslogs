package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class InfernalShrinesMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.INFERNAL_SHRINES.getMapName();
    private final String mapIcon = Maps.INFERNAL_SHRINES.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
