package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class HanamuraMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.HANAMURA.getMapName();
    private final String mapIcon = Maps.HANAMURA.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
