package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class GardenOfTerrorMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.GARDEN_OF_TERROR.getMapName();
    private final String mapIcon = Maps.GARDEN_OF_TERROR.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
