package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class TowersOfDoomMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.TOWERS_OF_DOOM.getMapName();
    private final String mapIcon = Maps.TOWERS_OF_DOOM.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
