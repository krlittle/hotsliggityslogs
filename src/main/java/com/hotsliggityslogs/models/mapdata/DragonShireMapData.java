package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class DragonShireMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.DRAGON_SHIRE.getMapName();
    private final String mapIcon = Maps.DRAGON_SHIRE.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
