package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class CursedHollowMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.CURSED_HOLLOW.getMapName();
    private final String mapIcon = Maps.CURSED_HOLLOW.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
