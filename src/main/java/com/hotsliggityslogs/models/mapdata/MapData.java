package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.models.matches.Match;

public interface MapData {
    int getWins();

    void setWins(int wins);

    int getLosses();

    void setLosses(int losses);

    int getMatches();

    void setMatches(int matches);

    Double getWinRate();

    void setWinRate(Double winRate);

    String getMapName();

    String getMapIcon();

    void addMapData(Match match);

    Double calculateWinRate(int wins, int matches);
}
