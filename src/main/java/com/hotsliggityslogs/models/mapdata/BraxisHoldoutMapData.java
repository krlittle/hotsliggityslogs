package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class BraxisHoldoutMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.BRAXIS_HOLDOUT.getMapName();
    private final String mapIcon = Maps.BRAXIS_HOLDOUT.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
