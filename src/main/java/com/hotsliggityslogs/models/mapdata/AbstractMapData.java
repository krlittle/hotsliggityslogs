package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.models.matches.Match;

import java.text.DecimalFormat;

public abstract class AbstractMapData implements MapData {
    private int wins;
    private int losses;
    private int matches;
    private Double winRate;

    @Override
    public int getWins() {
        return wins;
    }

    @Override
    public void setWins(int wins) {
        this.wins = wins;
    }

    @Override
    public int getLosses() {
        return losses;
    }

    @Override
    public void setLosses(int losses) {
        this.losses = losses;
    }

    @Override
    public int getMatches() {
        return matches;
    }

    @Override
    public void setMatches(int matches) {
        this.matches = matches;
    }

    @Override
    public Double getWinRate() {
        return winRate;
    }

    @Override
    public void setWinRate(Double winRate) {
        this.winRate = winRate;
    }

    @Override
    public void addMapData(Match match) {
        if (match.getMatchOutcome().equals("Win")) {
            wins++;
        } else {
            losses++;
        }
        this.matches++;
    }

    @Override
    public Double calculateWinRate(int wins, int matches) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        return Double.valueOf(decimalFormat.format(((double) wins / matches) * 100));
    }
}
