package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class BlackheartsBayMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.BLACKHEARTS_BAY.getMapName();
    private final String mapIcon = Maps.BLACKHEARTS_BAY.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
