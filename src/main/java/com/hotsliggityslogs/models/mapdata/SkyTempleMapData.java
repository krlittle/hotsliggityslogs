package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class SkyTempleMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.SKY_TEMPLE.getMapName();
    private final String mapIcon = Maps.SKY_TEMPLE.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
