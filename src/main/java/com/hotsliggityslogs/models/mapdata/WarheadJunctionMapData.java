package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class WarheadJunctionMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.WARHEAD_JUNCTION.getMapName();
    private final String mapIcon = Maps.WARHEAD_JUNCTION.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
