package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class TombOfTheSpiderQueenMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.TOMB_OF_THE_SPIDER_QUEEN.getMapName();
    private final String mapIcon = Maps.TOMB_OF_THE_SPIDER_QUEEN.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}
