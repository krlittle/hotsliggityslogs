package com.hotsliggityslogs.models.mapdata;

import com.hotsliggityslogs.enumerations.Maps;

public class BattlefieldOfEternityMapData extends AbstractMapData implements MapData {
    private final String mapName = Maps.BATTLEFIELD_OF_ETERNITY.getMapName();
    private final String mapIcon = Maps.BATTLEFIELD_OF_ETERNITY.getMapIconName();

    @Override
    public String getMapName() {
        return mapName;
    }

    @Override
    public String getMapIcon() {
        return mapIcon;
    }
}