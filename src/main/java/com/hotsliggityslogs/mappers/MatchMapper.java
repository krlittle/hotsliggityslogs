package com.hotsliggityslogs.mappers;

import com.hotsliggityslogs.models.matches.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MatchMapper {
    public Match map(MatchDocument matchDocument) {

        Match match = null;

        switch(matchDocument.getMapName()) {
            case "Battlefield of Eternity":
                match = new BattlefieldOfEternityMatch();
                break;
            case "Blackheart's Bay":
                match = new BlackheartsBayMatch();
                break;
            case "Braxis Holdout":
                match = new BraxisHoldoutMatch();
                break;
            case "Cursed Hollow":
                match = new CursedHollowMatch();
                break;
            case "Dragon Shire":
                match = new DragonShireMatch();
                break;
            case "Garden of Terror":
                match = new GardenOfTerrorMatch();
                break;
            case "Hanamura":
                match = new HanamuraMatch();
                break;
            case "Haunted Mines":
                match = new HauntedMinesMatch();
                break;
            case "Infernal Shrines":
                match = new InfernalShrinesMatch();
                break;
            case "Sky Temple":
                match = new SkyTempleMatch();
                break;
            case "Tomb of the Spider Queen":
                match = new TombOfTheSpiderQueenMatch();
                break;
            case "Towers of Doom":
                match = new TowersOfDoomMatch();
                break;
            case "Warhead Junction":
                match = new WarheadJunctionMatch();
                break;
            default:
                break;
        }

        match.setId(matchDocument.getId());
        match.setMatchType(matchDocument.getMatchType());
        match.setMapName(matchDocument.getMapName());
        match.setMatchLength(matchDocument.getMatchLength());
        match.setHeroName(matchDocument.getHeroName());
        match.setHeroLevel(matchDocument.getHeroLevel());
        match.setMatchmakingRating(matchDocument.getMatchmakingRating());
        match.setRatingAdjustmentPoints(matchDocument.getRatingAdjustmentPoints());
        match.setMatchDate(matchDocument.getMatchDate());
        match.setMatchTime(matchDocument.getMatchTime());
        match.setMatchOutcome(matchDocument.getMatchOutcome());
        match.setSeason(matchDocument.getSeason());

        return match;
    }

    public List<Match> map(List<MatchDocument> matchesDocuments) {
        List<Match> matches = new ArrayList<>();

        for (MatchDocument matchDocument : matchesDocuments) {
            matches.add(map(matchDocument));
        }

        return matches;
    }
}
