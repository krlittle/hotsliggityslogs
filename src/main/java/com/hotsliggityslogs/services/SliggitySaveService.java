package com.hotsliggityslogs.services;

import com.hotsliggityslogs.models.matches.MatchDocument;
import com.hotsliggityslogs.models.ProtocolMatch;
import com.hotsliggityslogs.models.requests.MatchesRequest;
import com.hotsliggityslogs.repository.SliggityProtocolMatchRepo;
import com.hotsliggityslogs.repository.SliggityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SliggitySaveService {

    @Autowired
    private SliggityRepo sliggityRepo;

    @Autowired
    private SliggityProtocolMatchRepo sliggityProtocolMatchRepo;

    public void save(MatchesRequest matchesRequest) {

        for(MatchDocument match : matchesRequest.getMatches()) {
            sliggityRepo.save(match);
        }
    }

    public void save(ProtocolMatch protocolMatch) {
        sliggityProtocolMatchRepo.save(protocolMatch);
    }
}
