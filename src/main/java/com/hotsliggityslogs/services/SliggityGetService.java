package com.hotsliggityslogs.services;

import com.hotsliggityslogs.factories.MatchesResponseFactory;
import com.hotsliggityslogs.mappers.MatchMapper;
import com.hotsliggityslogs.models.ProtocolMatch;
import com.hotsliggityslogs.models.matches.MatchDocument;
import com.hotsliggityslogs.models.responses.MatchesResponse;
import com.hotsliggityslogs.models.responses.ProtocolResponse;
import com.hotsliggityslogs.repository.SliggityProtocolMatchRepo;
import com.hotsliggityslogs.repository.SliggityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SliggityGetService {

    @Autowired
    private SliggityRepo sliggityRepo;
    @Autowired
    private SliggityProtocolMatchRepo sliggityProtocolMatchRepo;
    @Autowired
    private MatchesResponseFactory matchesResponseFactory;
    @Autowired
    private MatchMapper matchMapper;

    public MatchesResponse getMatchById(String id) {
        List<MatchDocument> matches = new ArrayList<>();
        matches.add(sliggityRepo.findOne(id));

        return matchesResponseFactory.createMatchesResponse(matchMapper.map(matches));
    }

    public MatchesResponse getMatch(String heroName, String beginningDate, String endDate, String matchType, String mapName) {
        Sort matchDateDescending = new Sort(Sort.Direction.DESC, "matchDate");

        List<MatchDocument> matches = new ArrayList<>();
        if (heroName.equals("All Heroes")) {
            if (matchType.equals("All Modes")) {
                if (mapName.equals("All Maps")) {
                    matches.addAll(sliggityRepo.findByDates(beginningDate, endDate, matchDateDescending));
                } else {
                    matches.addAll(sliggityRepo.findByMap(mapName, beginningDate, endDate, matchDateDescending));
                }
            } else {
                if (mapName.equals("All Maps")) {
                    matches.addAll(sliggityRepo.findByMatchType(matchType, beginningDate, endDate, matchDateDescending));
                } else {
                    matches.addAll(sliggityRepo.findByMatchTypeAndMap(matchType, mapName, beginningDate, endDate, matchDateDescending));
                }
            }
        } else {
            if (matchType.equals("All Modes")) {
                if (mapName.equals("All Maps")) {
                    matches.addAll(sliggityRepo.findByHero(heroName, beginningDate, endDate, matchDateDescending));
                } else {
                    matches.addAll(sliggityRepo.findByHeroAndMap(heroName, mapName, beginningDate, endDate, matchDateDescending));
                }
            } else {
                if (mapName.equals("All Maps")) {
                    matches.addAll(sliggityRepo.findByHeroAndMatchType(heroName, matchType, beginningDate, endDate, matchDateDescending));
                } else {
                    matches.addAll(sliggityRepo.findByHeroAndMatchTypeAndMap(heroName, matchType, mapName, beginningDate, endDate, matchDateDescending));
                }
            }
        }

        return matchesResponseFactory.createMatchesResponse(matchMapper.map(matches), heroName);
    }

    public ProtocolResponse getMatch(String mapName) {
        List<ProtocolMatch> protocolMatches = new ArrayList<>();
        protocolMatches.addAll(sliggityProtocolMatchRepo.findByMap(mapName));

        ProtocolResponse protocolResponse = new ProtocolResponse();
        protocolResponse.setProtocolMatches(protocolMatches);

        return protocolResponse;
    }
}
