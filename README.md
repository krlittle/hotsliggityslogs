# HotSliggitySlogs
![#005B00](https://placehold.it/15/005B00/000000?text=+) HotSliggitySlogs's <code>master</code> branch is working

A RESTful Spring Boot app that utilizes Spring Data to store and access Heroes of the Storm data sent by [HotSliggityBatch](https://github.com/krlittle/HotSliggityBatch)
- A MongoDB instance containing a database named <code>sliggity</code> must be running prior to starting this app
- HotSliggitySlogs starts up on port 8081 by default
- If you'd like to change the name of the database or port the app starts up on, this can be configured by updating the the <code>[application.properties](https://github.com/krlittle/HotSliggitySlogs/blob/master/src/main/resources/application.properties)</code> file

For the user interface, see [HotSliggityFace](https://github.com/krlittle/HotSliggityFace)
- HotSliggityFace starts up on port 8082 by default

For the batch load job, see [HotSliggityBatch](https://github.com/krlittle/HotSliggityBatch)
- HotSliggityBatch starts up on port 8083 by default
